#!/bin/sh


set -e 

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;' # runs nginx in the fore ground not background which it does by default
