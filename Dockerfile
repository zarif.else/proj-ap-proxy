FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="zarif.else@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

# default environmental variable

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000


# SWITCH TO ROOT USER TO MAKE CHANGES TO FILE SYSTEM, WE THEN SWITCH BACK TO NGINX USER AFTERWARDS

USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf




COPY ./entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

USER nginx

# command that's run on docker file 
CMD ["/entrypoint.sh"]